let posts = $('#my-posts');


$.ajax("/data/my-ajax-post.json").done(function(superheroesArray){
    let superheroHTML = "";

    superheroesArray.forEach(function(superhero) {
        superheroHTML += `<h1> ${superhero.title} </h1> <p> ${superhero.categories[0]} </p> <p> ${superhero.content} </p>`
    });

    console.log(superheroHTML)
    posts.html(superheroHTML)

});
